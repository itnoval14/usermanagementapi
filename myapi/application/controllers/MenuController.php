<?php 
class MenuController extends RestApi_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database(); // Memuat library database
    }

    public function index()
    {
        $data = $this->db->get('user')->result();
        $this->response(['success' => true, 'data' => $data]);
    }

    public function getmenu($id)
    {
        $menus = $this->db->select('menu_name, menu_link, menu_icon, parent_id')
                     ->from('menu')
                     ->join('menu_user', 'menu_user.menu_id = menu.menu_id')
                     ->where('menu_user.id_user', $id)
                     ->get()
                     ->result();

        if ($menus) {
            $this->response(['success' => true, 'data' => $menus]);
        } else {
            $this->response(['success' => false, 'message' => 'Menu tidak ditemukan'], 404);
        }
    }

}
?>