<?php
class AuthController extends RestApi_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database(); // Memuat library database
    }
    public function login()
    {        
        //validation
        $error = [];
        if( !$this->getPost('email')) $error[] = 'Username harus diisi';
        if( !$this->getPost('password')) $error[] = 'Password harus diisi';

        if( count($error) > 0 )
        {
            $this->response(['success' => false, 'message' => $error[0] ], 422);
        }

        $exist = $this->db->get_where('user', ['email' => $this->getPost('email')])->row();
        if( $exist )
        {
            if( password_verify($this->getPost('password'), $exist->password) )
            {
                $data = $this->db->select('email, nama_user, status_user, no_hp,wa,id_jenis_user')->get_where('user',['id_user' => $exist->id_user])->row();
                $this->response( ['success'=> true, 'data' => $data]);
            }else{

			    $this->response( ['success'=> false, 'message' => 'User tidak ditemukan' ] );
		    }
        }else{
            $this->response( ['success'=> false, 'message' => 'User tidak ditemukan' ], 404 );
        }
    }

}
?>