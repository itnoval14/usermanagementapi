<?php 
class UsersController extends RestApi_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database(); // Memuat library database
    }

    public function index()
    {
        $data = $this->db->get('user')->result();
        $this->response(['success' => true, 'data' => $data]);
    }

    public function show($id)
    {
        $data = $this->db->get_where('user', ['id_user' => $id])->row();
        $this->response(['success' => true, 'data' => $data]);
    }

    public function save()
    {
        //validation
        $error = [];
        if( !$this->getPost('email')) $error[] = 'email harus diisi';
        if( !$this->getPost('password')) $error[] = 'Password harus diisi';

        if( count($error) > 0 )
        {
            $this->response(['success' => false, 'message' => $error[0] ], 422);
        }
        $date = date('Y-m-d H:i:s');
        $insert = [
            'id_user' => $this->getID(),
            'nama_user' => $this->getPost('email'),
            'email' => $this->getPost('email'),
            'password' => password_hash($this->getPost('password'), PASSWORD_DEFAULT),
            'wa' => $this->getPost('wa'),
            'no_hp' => $this->getPost('no_hp'),
            'pin' => $this->getPost('pin'),
            'id_jenis_user' => $this->getPost('id_jenis_user'),
            'status_user' => $this->getPost('status_user'),
            'delete_mark' => $this->getPost('delete_mark'),
            'create_by' => "system",
            'create_date' => $date,
            'update_by' => null,
            'update_date' => null
        ];
        $this->db->insert('user', $insert);

        $this->response(['success' => true, 'message' => 'Berhasil insert user']);
    }

    public function update($id)
    {
        $update = [];

        if( $this->getPost('password') ) $update['password'] = password_hash($this->getPost('password'), PASSWORD_DEFAULT);
        if( $this->getPost('address') ) $update['address'] = $this->getPost('address');
        if( $this->getPost('phone') ) $update['phone'] = $this->getPost('phone');
        
        $this->db->update('m_users', $update, ['id' => $id]);

        $this->response(['success' => true, 'message' => 'Berhasil update user']);
    }
    
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('m_users');

        $this->response(['success' => true, 'message' => 'Berhasil delete user']);
    }

    function getID() {
        $characters = '0123456789';
        $randomString = '';
     
        for ($i = 0; $i < 3; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

     
        return $randomString;
    }

}
?>